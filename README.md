# Docker container for Observium Community Edition
[![pipeline status](https://gitlab.com/trithemius/observium/badges/master/pipeline.svg)](https://gitlab.com/trithemius/observium/commits/master)

Observium is network monitoring with intuition. It is a low-maintenance auto-discovering network monitoring platform supporting a wide range of device types, platforms and operating systems including Cisco, Windows, Linux, HP, Juniper, Dell, FreeBSD, Brocade, Netscaler, NetApp and many more. Observium focuses on providing a beautiful and powerful yet simple and intuitive interface to the health and status of your network. For more information, go to http://www.observium.org site.
This version is based on mbixtech/observium but is updated with PHP 7.3 and supports Mysql 8.x. 

## Usage
Either follow the choice A. or B. below to run Observium.

### A. Manual Run Containers
- Prepare working directory for docker containers, for example below.
```
  $ mkdir /home/docker/observium
  $ cd /home/docker/observium
  $ mkidr data logs rrd
```
- Run official MariaDB container
```
  $ docker run --name observiumdb
    -v /home/docker/observium/data:/var/lib/mysql \
    -e MYSQL_ROOT_PASSWORD=passw0rd \
    -e MYSQL_USER=observium \
    -e MYSQL_PASSWORD=passw0rd \
    -e MYSQL_DATABASE=observium
    -e TZ=Europe/Vienna
    mariadb
```

- Run this Observium container
```
  $ docker run --name observiumapp --link observiumdb:observiumdb \
    -v /home/docker/observium/logs:/opt/observium/logs \
    -v /home/docker/observium/rrd:/opt/observium/rrd \
    -e OBSERVIUM_ADMIN_USER=admin \
    -e OBSERVIUM_ADMIN_PASS=passw0rd \
    -e OBSERVIUM_DB_HOST=observiumdb \
    -e OBSERVIUM_DB_USER=observium \
    -e OBSERVIUM_DB_PASS=passw0rd \
    -e OBSERVIUM_DB_NAME=observium \
    -e OBSERVIUM_BASE_URL=http://yourserver.yourdomain:8080 \
    -e TZ=Europe/Vienna
    -p 8080:80
    trithemius/observium
```

> Note:
> - You must replace passwords specified in environment parameters of both containers with your secure passwords instead.

### B. Use Docker Composer

```
version: '3'

services:
  observiumdb:
       image: mysql:latest
       environment:
         - MYSQL_ROOT_PASSWORD=passw0rd
         - MYSQL_USER=observium
         - MYSQL_PASSWORD=passw0rd
         - MYSQL_DATABASE=observium
         - TZ=Europe/Vienna
       volumes:
         - /home/docker/observium/data:/var/lib/mysql
       networks:
         - back-tier
       restart: always

  observiumap:
       image: trithemius/observium
       environment:
         - OBSERVIUM_ADMIN_USER=admin
         - OBSERVIUM_ADMIN_PASS=passw0rd
         - OBSERVIUM_DB_HOST=observiumdb
         - OBSERVIUM_DB_NAME=observium
         - OBSERVIUM_DB_USER=observium 
         - OBSERVIUM_DB_PASS=passw0rd
         - OBSERVIUM_BASE_URL=http://observium.mbixtech.com:8888
         - TZ=Asia/Bangkok
       volumes:
         - /home/docker/observium/logs:/opt/observium/logs
         - /home/docker/observium/rrd:/opt/observium/rrd
       networks:
         - back-tier
       ports:
         - "8888:80"
       restart: always
       depends_on:
         - observiumdb

networks:
  back-tier:
      driver: bridge

```


## Changes
- upgraded PHP libraries and binaries to 7.3

## Source Repository
https://gitlab.com/trithemius/observium
